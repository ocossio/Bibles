﻿namespace Bibles.Helpers
{
    using Xamarin.Forms;
    using Interfaces;
    using Resources;

    public static class Languages
    {
        static Languages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }

        public static string Accept
        {
            get { return Resource.Accept; }
        }

        public static string Error
        {
            get { return Resource.Error; }
        }

        public static string EmailValidation
        {
            get { return Resource.EmailValidation; }
        }

        public static string PassValidation
        {
            get { return Resource.PassValidation; }
        }

        public static string SearchOne
        {
            get { return Resource.SearchOne; }
        }

        public static string SearchTwo
        {
            get { return Resource.SearchTwo; }
        }

        public static string LogOut
        {
            get { return Resource.LogOut; }
        }

        public static string EmailPlaceHolder
        {
            get { return Resource.EmailPlaceHolder; }
        }

        public static string Rememberme
        {
            get { return Resource.Rememberme; }
        }

        public static string Login
        {
            get { return Resource.Login; }
        }

        public static string PasswordPlaceHolder
        {
            get { return Resource.PasswordPlaceHolder; }
        }

        public static string ForgotPass
        {
            get { return Resource.ForgotPass; }
        }

        public static string Register
        {
            get { return Resource.Register; }
        }

        public static string ServiceError
        {
            get { return Resource.ServiceError; }
        }

        public static string Menu
        {
            get { return Resource.Menu; }
        }

        public static string Bibles
        {
            get { return Resource.Bibles; }
        }

        public static string Search
        {
            get { return Resource.Search; }
        }

        public static string Language
        {
            get { return Resource.Language; }
        }

        public static string Year
        {
            get { return Resource.Year; }
        }
         
        public static string BibleValidation
        {
            get { return Resource.BibleValidation; }
        }

        public static string BookValidation
        {
            get { return Resource.BookValidation; }
        }

        public static string ChapterValidation
        {
            get { return Resource.ChapterValidation; }
        }

        public static string NumberValidation
        {
            get { return Resource.NumberValidation; }
        }

        public static string MaxNumberValidation
        {
            get { return Resource.MaxNumberValidation; }
        }

        public static string NumberVerseValidation
        {
            get { return Resource.NumberVerseValidation; }
        }

        public static string KeyWordValidation
        {
            get { return Resource.KeyWordValidation; }
        }

        public static string LetterValidation
        {
            get { return Resource.LetterValidation; }
        }

        public static string VariousBooks
        {
            get { return Resource.VariousBooks; }
        }

        public static string Previous
        {
            get { return Resource.Previous; }
        }

        public static string Chapter
        {
            get { return Resource.Chapter; }
        }

        public static string Next
        {
            get { return Resource.Next; }
        }

        public static string SearchTitle
        {
            get { return Resource.SearchTitle; }
        }

        public static string RegisterTitle
        {
            get { return Resource.RegisterTitle; }
        }

        public static string ChangeImage
        {
            get { return Resource.ChangeImage; }
        }

        public static string FirstNameLabel
        {
            get { return Resource.FirstNameLabel; }
        }

        public static string FirstNamePlaceHolder
        {
            get { return Resource.FirstNamePlaceHolder; }
        }

        public static string FirstNameValidation
        {
            get { return Resource.FirstNameValidation; }
        }

        public static string LastNameLabel
        {
            get { return Resource.LastNameLabel; }
        }

        public static string LastNamePlaceHolder
        {
            get { return Resource.LastNamePlaceHolder; }
        }

        public static string LastNameValidation
        {
            get { return Resource.LastNameValidation; }
        }

        public static string PhoneLabel
        {
            get { return Resource.PhoneLabel; }
        }

        public static string PhonePlaceHolder
        {
            get { return Resource.PhonePlaceHolder; }
        }

        public static string PhoneValidation
        {
            get { return Resource.PhoneValidation; }
        }

        public static string ConfirmLabel
        {
            get { return Resource.ConfirmLabel; }
        }

        public static string ConfirmPlaceHolder
        {
            get { return Resource.ConfirmPlaceHolder; }
        }

        public static string ConfirmValidation
        {
            get { return Resource.ConfirmValidation; }
        }

        public static string EmailValidation2
        {
            get { return Resource.EmailValidation2; }
        }

        public static string PasswordValidation2
        {
            get { return Resource.PasswordValidation2; }
        }

        public static string ConfirmValidation2
        {
            get { return Resource.ConfirmValidation2; }
        }

        public static string UserRegisteredMessage
        {
            get { return Resource.UserRegisteredMessage; }
        }

        public static string SourceImageQuestion
        {
            get { return Resource.SourceImageQuestion; }
        }

        public static string Cancel
        {
            get { return Resource.Cancel; }
        }

        public static string FromGallery
        {
            get { return Resource.FromGallery; }
        }

        public static string FromCamera
        {
            get { return Resource.FromCamera; }
        }
    }
}