﻿namespace Bibles.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Models;
    using Helpers;

    public class MainViewModel
    {
        #region Attributes
        private static MainViewModel instance;
        #endregion

        #region Properties
        public string SelectChapter
        {
            get;
            set;
        }

        public string LangShort
        {
            get;
            set;
        }

        public string SelectedModule
        {
            get;
            set;
        }

        public string SelectedShortName
        {
            get;
            set;
        }

        public string SelectedNameBook
        {
            get;
            set;
        }

        public string Token 
        { 
            get; 
            set; 
        }

        public string TokenType 
        { 
            get; 
            set; 
        }

        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }

        public ObservableCollection<BaseVerse> SearchBooks
        {
            get;
            set;
        }

        public object _Bibles
        {
            get;
            set;
        }
        #endregion

        #region ViewModels
        public LoginViewModel Login
        {
            get;
            set;
        }

        public BiblesViewModel Bibles
        {
            get;
            set;
        }

        public BooksViewModel Books
        {
            get;
            set;
        }

        public ChapterViewModel Chapter
        {
            get;
            set;
        }

        public SearchOneViewModel SearchOne
        {
            get;
            set;
        }

        public SearchTwoViewModel SearchTwo
        {
            get;
            set;
        }

        public ChapterSearchViewModel ChapterSearch
        {
            get;
            set;
        }

        public KeywordSearchViewModel KeywordSearch
        {
            get;
            set;
        }

        public RegisterViewModel Register
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
            this.LoadMenu();
        }
        #endregion

        #region Singleton
        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }
        #endregion

        #region Methods
        private void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();
            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_stat_search",
                PageName = "SearchOnePage",
                Title = Languages.SearchOne,
            });

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_stat_youtube_searched_for",
                PageName = "SearchTwoPage",
                Title = Languages.SearchTwo,
            });

            this.Menus.Add(new MenuItemViewModel
            {
                Icon = "ic_stat_power_settings_new",
                PageName = "LoginPage",
                Title = Languages.LogOut,
            });
        }
        #endregion
    }
}