﻿namespace Bibles.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using Bibles.Helpers;
    using Bibles.Views;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BiblesViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private ObservableCollection<BibleItemViewModel> bibles;
        private List<Bible> myBibles;
        private List<Bibles> biblesList;
        private bool isRefreshing;
        private string filter;
        #endregion

        #region Properties
        public ObservableCollection<BibleItemViewModel> Bibles
        {
            get { return this.bibles; }
            set { SetValue(ref this.bibles, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }

        public string Filter
        {
            get { return this.filter; }
            set
            {
                this.SetValue(ref this.filter, value);
                this.Search();
            }
        }
        #endregion

        #region Constructors
        public BiblesViewModel()
        {
            this.apiService = new ApiService();
            this.LoadBibles();
        }
        #endregion

        #region Methods
        private async void LoadBibles()
        {
            this.IsRefreshing = true;
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                //await Application.Current.MainPage.Navigation.PopAsync();
                Application.Current.MainPage = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex("#2c3e50"),
                    BarTextColor = Color.White,
                };
                return;
            }

            var apiBibles = Application.Current.Resources["APIBibles"].ToString();
            var response = await this.apiService.GetList<Bibles>(
                apiBibles,
                "/api",
                "/bibles");

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                //await Application.Current.MainPage.Navigation.PopAsync();
                Application.Current.MainPage = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex("#2c3e50"),
                    BarTextColor = Color.White,
                };
                return;
            }

            this.biblesList = (List<Bibles>)response.Result;

            var type = typeof(BiblesResults);
            var properties = type.GetRuntimeFields();
            this.myBibles = new List<Bible>();

            foreach (var property in properties)
            {
                var code = property.Name.Substring(1, 3);
                var _bible = (Bible)property.GetValue(this.biblesList.ElementAt(0).Results);
                this.myBibles.Add(_bible);
            }

            this.Bibles = new ObservableCollection<BibleItemViewModel>(
                this.ToBibleItemViewModel());
            this.IsRefreshing = false;
        }

        private IEnumerable<BibleItemViewModel> ToBibleItemViewModel()
        {
            return this.myBibles.Select(l => new BibleItemViewModel
            {
                Copyright = l.Copyright,
                Italics = l.Italics,
                Lang = l.Lang,
                Lang_Short = l.Lang_Short,
                Module = l.Module,
                Name = l.Name,
                Rank = l.Rank,
                Research = l.Research,
                ShortName = l.ShortName,
                Strongs = l.Strongs,
                Year = l.Year,
            }).ToList();
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.Bibles = new ObservableCollection<BibleItemViewModel>(
                    this.ToBibleItemViewModel());
            }
            else
            {
                this.Bibles = new ObservableCollection<BibleItemViewModel>(
                    this.ToBibleItemViewModel().Where(
                        l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                             l.Lang.ToLower().Contains(this.Filter.ToLower()) ||
                             l.Year.ToLower().Contains(this.Filter.ToLower())));
            }
        }
        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadBibles);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }
        #endregion
    }
}

