﻿namespace Bibles.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;
    using Bible.ViewModels;
    using Bibles.Helpers;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class BooksViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private Bible baseBible;
        private List<Book> myBooks;
        private bool isRefreshing;
        private List<Books> booksResult;
        private ObservableCollection<BookItemViewModel> books;
        private BookItemViewModel bookItemViewModel;
        private BibleItemViewModel bibleItemViewModel;
        private string filter;
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public Bible BaseBible
        {
            get;
            set;
        }

        public ObservableCollection<BookItemViewModel> Books
        {
            get { return this.books; }
            set { SetValue(ref this.books, value); }
        }

        public string Filter
        {
            get { return this.filter; }
            set
            {
                this.SetValue(ref this.filter, value);
                this.Search();
            }
        }
        #endregion

        #region Constructors
        public BooksViewModel(Bible baseBible)
        {
            this.apiService = new ApiService();
            this.BaseBible = baseBible;
            this.LoadBooks();
        }

        public BooksViewModel(BookItemViewModel bookItemViewModel)
        {
            this.bookItemViewModel = bookItemViewModel;
        }
        #endregion

        #region Methods
        private async void LoadBooks()
        {
            this.IsRefreshing = true;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }

            var apiBibles = Application.Current.Resources["APIBibles"].ToString();
            var response = await this.apiService.GetList<Books>(
                apiBibles,
                "/api",
                string.Format("/books?language={0}", BaseBible.Lang_Short));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            this.booksResult = (List<Books>)response.Result;

            if (BaseBible.Lang_Short != "en")
            {
                var response2 = await this.apiService.GetList<Books>(
                    apiBibles,
                    "/api",
                    "/books?language=en");

                if (!response2.IsSuccess)
                {
                    this.IsRefreshing = false;
                    await Application.Current.MainPage.DisplayAlert(
                        Languages.Error,
                        response2.Message,
                        Languages.Accept);
                    return;
                }

                var booksResult2 = (List<Books>)response2.Result;

                for (int i = 0; i < this.booksResult.ElementAt(0).Results.Count; i++)
                {
                    this.booksResult.ElementAt(0).Results[i].ShortName = booksResult2.ElementAt(0).Results[i].ShortName;
                }
            }

            this.Books = new ObservableCollection<BookItemViewModel>(
                this.ToBookItemViewModel());
            this.IsRefreshing = false;
        }

        private IEnumerable<BookItemViewModel> ToBookItemViewModel()
        {
            return this.booksResult.ElementAt(0).Results.Select(l => new BookItemViewModel
            {
                Id = l.Id,
                Name = l.Name,
                ShortName = l.ShortName,
            });
        }

        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                this.Books = new ObservableCollection<BookItemViewModel>(
                    this.ToBookItemViewModel());
            }
            else
            {
                this.Books = new ObservableCollection<BookItemViewModel>(
                    this.ToBookItemViewModel().Where(
                        l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                        l.ShortName.ToLower().Contains(this.Filter.ToLower())));
            }
        }
        #endregion

        #region Commands
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(LoadBooks);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }
        #endregion
    }
}

