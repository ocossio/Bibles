﻿namespace Bibles.ViewModels
{
    using System.Collections.ObjectModel;
    using Models;

    public class ChapterSearchViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<BaseVerse> verses;
        private SearchOneViewModel searchOneViewModel;
        #endregion

        #region Properties
        public ObservableCollection<BaseVerse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }
        #endregion

        #region Constructors
        public ChapterSearchViewModel(SearchOneViewModel searchOneViewModel)
        {
            this.searchOneViewModel = searchOneViewModel;
            this.ShowVerses();
        }
        #endregion

        #region Methods
        private void ShowVerses()
        {
            this.Verses = searchOneViewModel.Verses;
        }
        #endregion
    }
}

