﻿namespace Bibles.ViewModels
{
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Bibles.Views;
    using Xamarin.Forms;
    using Bibles.Services;
    using Bibles.Helpers;
    using Bibles.Models;
    using System;

    public class LoginViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private string email;
        private string password;
        private bool isRunning;
        private bool isEnabled;
        private TokenResponse token;
        #endregion

        #region Properties
        public string Email
        {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }

        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsRemembered
        {
            get;
            set;
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }
        #endregion

        #region Constructors
        public LoginViewModel()
        {
            this.apiService = new ApiService();
            this.IsRemembered = true;
            this.isEnabled    = true;
            this.Email = "oscarcossio75656@correo.itm.edu.co";
            this.Password = "123456";
        }
        #endregion

        #region Methods
        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.EmailValidation,
                    Languages.Accept);
                return;
            }
            else if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.PassValidation,
                    Languages.Accept);
                return;
            }
            else
            {
                this.IsRunning = true;
                this.isEnabled = false;
            }
       
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }

            var apiSecurity1 = Application.Current.Resources["APISecurity1"].ToString();
            this.token = await this.apiService.GetToken(
                apiSecurity1,
                this.Email,
                this.Password);

            if (this.token == null)
            {
                var apiSecurity2 = Application.Current.Resources["APISecurity2"].ToString();
                this.token = await this.apiService.GetToken(
                    apiSecurity2,
                    this.Email,
                    this.Password);
                
                if (this.token == null)
                {
                    this.IsRunning = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert(
                        Languages.Error,
                        Languages.ServiceError,
                        Languages.Accept);
                    return;
                }
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    token.ErrorDescription,
                    Languages.Accept);
                this.Password = string.Empty;
                return;
            }

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token.AccessToken;
            mainViewModel.TokenType = token.TokenType;

            if (this.IsRemembered)
            {
                Settings.Token = token.AccessToken;
                Settings.TokenType = token.TokenType;
            }

            mainViewModel.Bibles = new BiblesViewModel();
            Application.Current.MainPage = new MasterPage();
      
            this.IsRunning = false;
            this.isEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;
        }

        private async void Register()
        {
            MainViewModel.GetInstance().Register = new RegisterViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new RegisterPage());
        }
        #endregion

        #region Commands
        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        public ICommand RegisterCommand
        {
            get
            {
                return new RelayCommand(Register);
            }
        }
        #endregion
    } 
}

