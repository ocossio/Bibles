﻿namespace Bibles.ViewModels
{
    using System.Collections.ObjectModel;
    using Models;

    public class KeywordSearchViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<BaseVerse> verses;
        private SearchTwoViewModel searchTwoViewModel;
        #endregion

        #region Properties
        public ObservableCollection<BaseVerse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }
        #endregion

        #region Constructors
        public KeywordSearchViewModel(SearchTwoViewModel searchTwoViewModel)
        {
            this.searchTwoViewModel = searchTwoViewModel;
            this.ShowVerses();
        }
        #endregion

        #region Methods
        private void ShowVerses()
        {
            this.Verses = searchTwoViewModel.Verses;
        }
        #endregion
    }
}

