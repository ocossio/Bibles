﻿namespace Bible.ViewModels
{
    using System.Windows.Input;
    using Bibles;
    using Bibles.Models;
    using Bibles.ViewModels;
    using Bibles.Views;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;

    public class BookItemViewModel : Book
    {
        #region Commands
        public ICommand SelectBookCommand
        {
            get
            {
                return new RelayCommand(SelectBook);
            }
        }
        #endregion

        #region Methods
        private async void SelectBook()
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Chapter = new ChapterViewModel(this);
            mainViewModel.SelectedNameBook = Name;
            await App.Navigator.PushAsync(new ChapterPage());
        }
        #endregion
    }
}