﻿namespace Bibles.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows.Input;
    using Bibles.Views;
    using System.Text.RegularExpressions;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;
    using Bibles.Helpers;

    public class SearchOneViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private bool isRefreshing;
        private bool isRunning;
        private bool isEnabled;
        private string bible;
        private string book;
        private string chapter;
        private string verseStart;
        private string verseEnd;
        private ObservableCollection<Bible> bibles;
        private List<Bible> myBibles;
        private List<Bibles> biblesList;
        private string selectedModule;
        private string selectedLangShort;
        private List<Book> myBooks;
        private List<Books> booksResult;
        private ObservableCollection<Book> books;
        private List<BaseVerse> myVerses;
        private ObservableCollection<BaseVerse> verses;
        private List<Content> contentList;
        private static SearchOneViewModel instance;
        #endregion

        #region Properties
        public ObservableCollection<Bible> Bibles
        {
            get { return this.bibles; }
            set { SetValue(ref this.bibles, value); }
        }

        public ObservableCollection<Book> Books
        {
            get { return this.books; }
            set { SetValue(ref this.books, value); }
        }

        public ObservableCollection<BaseVerse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }

        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { this.SetValue(ref this.isRefreshing, value); }
        }

        public string SelectBible
        {
            get { return this.bible; }
            set { SetValue(ref this.bible, value); }
        }

        public string SelectBook
        {
            get { return this.book; }
            set { SetValue(ref this.book, value); }
        }

        public string Chapter
        {
            get { return this.chapter; }
            set { SetValue(ref this.chapter, value); }
        }

        public string VerseStart
        {
            get { return this.verseStart; }
            set { SetValue(ref this.verseStart, value); }
        }

        public string VerseEnd
        {
            get { return this.verseEnd; }
            set { SetValue(ref this.verseEnd, value); }
        }

        public Picker PickerBooks
        {
            get;
            set;
        }

        public string LangShort
        {
            get;
            set;
        }

        public string SelectedLangShort
        {
            get;
            set;
        }

        public string SelectedModule
        {
            get;
            set;
        }

        public string SelectedShortName
        {
            get;
            set;
        }
        #endregion

        #region ViewModels
        public SearchOneViewModel SearchOne
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public SearchOneViewModel()
        {
            instance = this;
            this.apiService = new ApiService();
            this.isEnabled = true;
            this.LoadPickerBibles();
        }

        public SearchOneViewModel(Picker pickerBooks, string sModule, string sLangShort)
        {
            this.SelectedModule = sModule;
            this.SelectedLangShort = sLangShort;
            this.PickerBooks = pickerBooks;
            this.apiService = new ApiService();
            this.isEnabled = true;
            this.LoadPickerBooks();
        }
        #endregion

        #region Methods
        private async void LoadPickerBibles()
        {
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                //await Application.Current.MainPage.Navigation.PopAsync();
                Application.Current.MainPage = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex("#2c3e50"),
                    BarTextColor = Color.White,
                };
                return;
            }

            var apiBibles = Application.Current.Resources["APIBibles"].ToString();
            var response = await this.apiService.GetList<Bibles>(
                apiBibles,
                "/api",
                "/bibles");

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                //await Application.Current.MainPage.Navigation.PopAsync();
                Application.Current.MainPage = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex("#2c3e50"),
                    BarTextColor = Color.White,
                };
                return;
            }

            this.biblesList = (List<Bibles>)response.Result;

            var type = typeof(BiblesResults);
            var properties = type.GetRuntimeFields();
            this.myBibles = new List<Bible>();

            foreach (var property in properties)
            {
                var code = property.Name.Substring(1, 3);
                var _bible = (Bible)property.GetValue(this.biblesList.ElementAt(0).Results);
                this.myBibles.Add(_bible);
            }

            this.Bibles = new ObservableCollection<Bible>(
                this.ToBibleItemViewModel());
        }

        private IEnumerable<Bible> ToBibleItemViewModel()
        {
            return this.myBibles.Select(l => new Bible
            {
                Copyright = l.Copyright,
                Italics = l.Italics,
                Lang = l.Lang,
                Lang_Short = l.Lang_Short,
                Module = l.Module,
                Name = l.Name,
                Rank = l.Rank,
                Research = l.Research,
                ShortName = l.ShortName,
                Strongs = l.Strongs,
                Year = l.Year,
            }).ToList();
        }

        private async void LoadPickerBooks()
        {
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            var apiBibles = Application.Current.Resources["APIBibles"].ToString();
            var response = await this.apiService.GetList<Books>(
                apiBibles,
                "/api",
                string.Format("/books?language={0}", selectedLangShort));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            this.booksResult = (List<Books>)response.Result;

            if (selectedLangShort != "en")
            {
                var response2 = await this.apiService.GetList<Books>(
                    apiBibles,
                    "/api",
                    "/books?language=en");

                if (!response2.IsSuccess)
                {
                    this.IsRefreshing = false;
                    await Application.Current.MainPage.DisplayAlert(
                        Languages.Error,
                        response2.Message,
                        Languages.Accept);
                    return;
                }

                var booksResult2 = (List<Books>)response2.Result;

                for (int i = 0; i < this.booksResult.ElementAt(0).Results.Count; i++)
                {
                    this.booksResult.ElementAt(0).Results[i].ShortName = booksResult2.ElementAt(0).Results[i].ShortName;
                }
            }

            this.Books = new ObservableCollection<Book>(
                this.ToGetBook());

            this.PickerBooks.ItemsSource = this.Books;
            this.PickerBooks.ItemDisplayBinding = new Binding("Name");
        }

        private IEnumerable<Book> ToGetBook()
        {
            return this.booksResult.ElementAt(0).Results.Select(l => new Book
            {
                Id = l.Id,
                Name = l.Name,
                ShortName = l.ShortName,
            });
        }

        private async void LoadSearch()
        {
            var connection = await this.apiService.CheckConnection();

            if (string.IsNullOrEmpty(this.SelectedModule))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.BibleValidation,
                    Languages.Accept);
                return;
            }
            else if (string.IsNullOrEmpty(this.SelectedShortName))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.BookValidation,
                    Languages.Accept);
                return;
            }
            else if (string.IsNullOrEmpty(this.Chapter))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.ChapterValidation,
                    Languages.Accept);
                return;
            }
            else if (!Regex.IsMatch(this.Chapter, "^[0-9]*$"))
            { 
                this.Chapter = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NumberValidation,
                    Languages.Accept);
                return;
            }
            else if (this.Chapter.Length > 2)
            {
                this.Chapter = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.MaxNumberValidation,
                    Languages.Accept);
                return;
            }
            else if (string.IsNullOrEmpty(this.VerseStart))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NumberVerseValidation,
                    Languages.Accept);
                return;
            }
            else if (!Regex.IsMatch(this.VerseStart, "^[0-9]*$"))
            {
                this.VerseStart = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NumberValidation,
                    Languages.Accept);
                return;
            }
            else if (this.VerseStart.Length > 2)
            {
                this.VerseStart = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.MaxNumberValidation,
                    Languages.Accept);
                return;
            }
            else if (string.IsNullOrEmpty(this.VerseEnd))
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NumberVerseValidation,
                    Languages.Accept);
                return;
            }
            else if (!Regex.IsMatch(this.VerseEnd, "^[0-9]*$"))
            {
                this.VerseEnd = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.NumberValidation,
                    Languages.Accept);
                return;
            }
            else if (this.VerseEnd.Length > 2)
            {
                this.VerseEnd = string.Empty;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    Languages.MaxNumberValidation,
                    Languages.Accept);
                return;
            }
            else
            {
                this.IsRunning = true;
                this.isEnabled = false;
            }

            if (!connection.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            var response = await this.apiService.GetList<Content>(
             "http://api.biblesupersearch.com",
             "/api",
                string.Format("?bible={0}&reference={1} {2}:{3}-{4}",
                 SelectedModule,
                 SelectedShortName,
                 Chapter,
                 VerseStart,
                 VerseEnd));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            this.contentList = (List<Content>)response.Result;
            var contentResult = contentList[0].ContentResults[0];

            var type_bibles = typeof(VerseResult);
            var properties_bibles = type_bibles.GetRuntimeFields();
            Quote _Quote = null;

            foreach (var property in properties_bibles)
            {
                var code = property.Name.Substring(1, 3);
                _Quote = (Quote)property.GetValue(contentResult.Verses);
                if (_Quote != null)
                {
                    break;
                }
            }

            if (_Quote == null)
            {
                return;
            }

            var type_chapter = typeof(Chapter);
            var properties_chapter = type_chapter.GetRuntimeFields();
            this.myVerses = new List<BaseVerse>();
            foreach (var property in properties_chapter)
            {
                if (this.Chapter == "1")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter1);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "2")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter2);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "3")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter3);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "4")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter4);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "5")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter5);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "6")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter6);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "7")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter7);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "8")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter8);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "9")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter9);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "10")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter10);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "12")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter12);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "13")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter13);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "14")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter14);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "15")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter15);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "16")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter16);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "17")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter17);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "18")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter18);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "19")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter19);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "20")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter20);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "21")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter21);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "22")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter22);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "23")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter23);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "24")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter24);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "25")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter25);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "26")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter26);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "27")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter27);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "28")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter28);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "29")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter29);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "30")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter30);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "31")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter31);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "32")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter32);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "33")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter33);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "34")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter34);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "35")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter35);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "36")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter36);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "37")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter37);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "38")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter38);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "39")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter39);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "40")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter40);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "41")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter41);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "42")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter42);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "43")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter43);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "44")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter44);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "45")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter45);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "46")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter46);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "47")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter47);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "48")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter48);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "49")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter49);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "50")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter50);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "51")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter51);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "52")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter52);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "53")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter53);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "54")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter54);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "55")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter55);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "56")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter56);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "57")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter57);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "58")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter58);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "59")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter59);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "60")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter60);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "61")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter61);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "62")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter62);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "63")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter63);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "64")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter64);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "65")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter65);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "66")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter66);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "67")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter67);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "68")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter68);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "69")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter69);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "70")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter70);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "71")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter71);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "72")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter72);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "73")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter73);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "74")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter74);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "75")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter75);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "76")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter76);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "77")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter77);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "78")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter78);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "79")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter79);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "80")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter80);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "81")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter81);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "82")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter82);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "83")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter83);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "84")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter84);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "85")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter85);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "86")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter86);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "87")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter87);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "88")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter88);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "89")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter89);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "90")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter90);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "91")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter91);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "92")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter92);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "93")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter93);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "94")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter94);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "95")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter95);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "96")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter96);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "97")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter97);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "98")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter98);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "99")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter99);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.Chapter == "100")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter100);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }
            }

            this.Verses = new ObservableCollection<BaseVerse>(this.myVerses);
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.ChapterSearch = new ChapterSearchViewModel(this);
            mainViewModel.SelectedNameBook = this.SelectBook;
            mainViewModel.SearchBooks = this.Verses;
            await App.Navigator.PushAsync(new ChapterSearchPage());

            this.myVerses.Clear();
            this.Chapter = string.Empty;
            this.VerseStart = string.Empty;
            this.VerseEnd = string.Empty;
            this.IsRunning = false;
            this.IsRefreshing = false;
        }
        #endregion

        #region Commands
        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(LoadSearch);
            }
        }
        #endregion

        #region Singleton
        public static SearchOneViewModel GetInstance()
        {
            if (instance == null)
            {
                return new SearchOneViewModel();
            }
            return instance;
        }
        #endregion
    }
}

