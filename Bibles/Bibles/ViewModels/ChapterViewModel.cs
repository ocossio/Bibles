﻿namespace Bibles.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reflection;
    using System.Text;
    using Bibles.Helpers;
    using System.Text.RegularExpressions;
    using System.Windows.Input;
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Services;
    using Xamarin.Forms;

    public class ChapterViewModel : BaseViewModel
    {
        #region Services
        private ApiService apiService;
        #endregion

        #region Attributes
        private Book book;
        private bool isRefreshing;
        private List<Content> contentList;
        private List<BaseVerse> myVerses;
        private ObservableCollection<BaseVerse> verses;
        private string prevChapter;
        private string nextChapter;
        private string pChapter;
        private string nChapter;
        private string bookName;
        private bool isEnabledBack;
        private bool isEnabledNext;
        private int p_Chapter_int;
        private int n_Chapter_int;
        #endregion

        #region Properties
        public bool IsRefreshing
        {
            get { return this.isRefreshing; }
            set { SetValue(ref this.isRefreshing, value); }
        }

        public ObservableCollection<BaseVerse> Verses
        {
            get { return this.verses; }
            set { SetValue(ref this.verses, value); }
        }

        public string Prev_Chapter
        {
            get { return this.prevChapter; }
            set { SetValue(ref this.prevChapter, value); }
        }

        public string Next_Chapter
        {
            get { return this.nextChapter; }
            set { SetValue(ref this.nextChapter, value); }
        }

        public string P_Chapter
        {
            get { return this.pChapter; }
            set { SetValue(ref this.pChapter, value); }
        }

        public string N_Chapter
        {
            get { return this.nChapter; }
            set { SetValue(ref this.nChapter, value); }
        }

        public string Book_Name
        {
            get { return this.bookName; }
            set { SetValue(ref this.bookName, value); }
        }

        public bool IsEnabledBack
        {
            get { return this.isEnabledBack; }
            set { SetValue(ref this.isEnabledBack, value); }
        }

        public bool IsEnabledNext
        {
            get { return this.isEnabledNext; }
            set { SetValue(ref this.isEnabledNext, value); }
        }
        #endregion

        #region Constructors
        public ChapterViewModel(Book book)
        {
            this.IsEnabledBack = true;
            this.isEnabledNext = true;
            this.apiService = new ApiService();
            this.book = book;
            this.LoadContent();
        }
        #endregion

        #region Methods
        private async void LoadContent()
        {
            this.IsRefreshing = true;
            this.IsEnabledBack = false;
            this.IsEnabledNext = false;

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    connection.Message,
                    Languages.Accept);
                return;
            }

            var apiBibles = Application.Current.Resources["APIBibles"].ToString();
            var response = await this.apiService.GetList<Content>(
                apiBibles,
                "/api",
                string.Format(
                    "?bible={0}&reference={1}", 
                    MainViewModel.GetInstance().SelectedModule, 
                    this.book.ShortName));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    Languages.Error,
                    response.Message,
                    Languages.Accept);
                return;
            }

            this.contentList = (List<Content>)response.Result;
            var contentResult = contentList[0].ContentResults[0];

            this.P_Chapter = contentList[0].ContentResults[0].Nav.PChapter;
            this.N_Chapter = contentList[0].ContentResults[0].Nav.NChapter;
            this.Prev_Chapter = contentList[0].ContentResults[0].Nav.PrevChapter;
            this.Next_Chapter = contentList[0].ContentResults[0].Nav.NextChapter;
            this.Book_Name = contentList[0].ContentResults[0].BookName;

            if (this.Prev_Chapter == null)
            {
                this.IsEnabledBack = false;
            }

            if (this.Next_Chapter == null)
            {
                this.IsEnabledNext = false;
            }

            var type_bibles = typeof(VerseResult);
            var properties_bibles = type_bibles.GetRuntimeFields();
            Quote _Quote = null;

            foreach (var property in properties_bibles)
            {
                var code = property.Name.Substring(1, 3);
                _Quote = (Quote)property.GetValue(contentResult.Verses);
                if (_Quote != null)        
                {
                    break;
                }
            }

            if (_Quote == null)
            {
                return;
            }

            var type_chapter = typeof(Chapter);
            var properties_chapter = type_chapter.GetRuntimeFields();
            this.myVerses = new List<BaseVerse>();
            foreach (var property in properties_chapter)
            {
                if (MainViewModel.GetInstance().SelectChapter == "1")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter1);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }
            }

            this.Verses = new ObservableCollection<BaseVerse>(this.myVerses);
            this.IsRefreshing = false;

            this.P_Chapter = contentList[0].ContentResults[0].Nav.PChapter;
            this.N_Chapter = contentList[0].ContentResults[0].Nav.NChapter;
            this.Prev_Chapter = contentList[0].ContentResults[0].Nav.PrevChapter;
            this.Next_Chapter = contentList[0].ContentResults[0].Nav.NextChapter;
            if (this.Prev_Chapter != null)
            {
                this.p_Chapter_int = Int32.Parse(P_Chapter);
            }
            if (this.Next_Chapter != null)
            {
               this.n_Chapter_int = Int32.Parse(N_Chapter);
            }

            if (this.Prev_Chapter == null)
            {
                this.IsEnabledBack = false;
            }
            else if (this.p_Chapter_int > this.n_Chapter_int)
            {
                this.IsEnabledBack = false;
            }
            else
            {
                this.IsEnabledBack = true;
            }

            if (this.Next_Chapter == null)
            {
                this.IsEnabledNext = false;
            }
            else
            {
                this.IsEnabledNext = true;
            }
        }

        private async void LoadChapterNext()
        {
            this.IsRefreshing = true;
            this.IsEnabledBack = false;
            this.IsEnabledNext = false;

            var reg = new Regex("[^a-zA-Z0-9 ]");
            var textNormalize = this.Next_Chapter.Normalize(NormalizationForm.FormD);
            string textReplaceNexthapter = reg.Replace(textNormalize, "");

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var response = await this.apiService.GetList<Content>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format(
                    "?bible={0}&reference={1}",
                    MainViewModel.GetInstance().SelectedModule,
                    textReplaceNexthapter));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            this.contentList = (List<Content>)response.Result;
            var contentResult = contentList[0].ContentResults[0];

            var type_bibles = typeof(VerseResult);
            var properties_bibles = type_bibles.GetRuntimeFields();
            Quote _Quote = null;

            foreach (var property in properties_bibles)
            {
                var code = property.Name.Substring(1, 3);
                _Quote = (Quote)property.GetValue(contentResult.Verses);
                if (_Quote != null)
                {
                    break;
                }
            }

            if (_Quote == null)
            {
                return;
            }

            var type_chapter = typeof(Chapter);
            var properties_chapter = type_chapter.GetRuntimeFields();
            this.myVerses = new List<BaseVerse>();
            foreach (var property in properties_chapter)
            {
                if (this.N_Chapter == " 1")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter1);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "1")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter1);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }


                if (this.N_Chapter == "2")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter2);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "3")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter3);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "4")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter4);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "5")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter5);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "6")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter6);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "7")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter7);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "8")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter8);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "9")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter9);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "10")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter10);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "12")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter12);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "13")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter13);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "14")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter14);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "15")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter15);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "16")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter16);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "17")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter17);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "18")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter18);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "19")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter19);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "20")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter20);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "21")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter21);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "22")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter22);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "23")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter23);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "24")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter24);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "25")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter25);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "26")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter26);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "27")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter27);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "28")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter28);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "29")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter29);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "30")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter30);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "31")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter31);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "32")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter32);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "33")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter33);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "34")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter34);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "35")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter35);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "36")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter36);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "37")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter37);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "38")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter38);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "39")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter39);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "40")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter40);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "41")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter41);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "42")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter42);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "43")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter43);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "44")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter44);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "45")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter45);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "46")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter46);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "47")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter47);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "48")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter48);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "49")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter49);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "50")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter50);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "51")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter51);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "52")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter52);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "53")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter53);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "54")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter54);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "55")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter55);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "56")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter56);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "57")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter57);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "58")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter58);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "59")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter59);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "60")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter60);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "61")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter61);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "62")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter62);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "63")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter63);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "64")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter64);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "65")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter65);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "66")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter66);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "67")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter67);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "68")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter68);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "69")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter69);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "70")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter70);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "71")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter71);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "72")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter72);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "73")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter73);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "74")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter74);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "75")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter75);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "76")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter76);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "77")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter77);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "78")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter78);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "79")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter79);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "80")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter80);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "81")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter81);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "82")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter82);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "83")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter83);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "84")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter84);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "85")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter85);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "86")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter86);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "87")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter87);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "88")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter88);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "89")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter89);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "90")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter90);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "91")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter91);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "92")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter92);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "93")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter93);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "94")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter94);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "95")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter95);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "96")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter96);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "97")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter97);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "98")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter98);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "99")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter99);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "100")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter100);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }
            }

            this.Verses = new ObservableCollection<BaseVerse>(this.myVerses);
            this.IsRefreshing = false;

            this.P_Chapter = contentList[0].ContentResults[0].Nav.PChapter;
            this.N_Chapter = contentList[0].ContentResults[0].Nav.NChapter;
            this.Prev_Chapter = contentList[0].ContentResults[0].Nav.PrevChapter;
            this.Next_Chapter = contentList[0].ContentResults[0].Nav.NextChapter;

            if (this.Prev_Chapter == null)
            {
                this.IsEnabledBack = false;
            }
            else
            {
                this.IsEnabledBack = true;
            }

            if (this.Next_Chapter == null)
            {
                this.IsEnabledNext = false;
            }
            else if (this.N_Chapter == " 1")
            {
                this.IsEnabledNext = false;
            }
            else
            {
                this.IsEnabledNext = true;
            }
        }

        private async void LoadChapterBack()
        {
            this.IsRefreshing = true;
            this.IsEnabledBack = false;
            this.IsEnabledNext = false;

            var reg = new Regex("[^a-zA-Z0-9 ]");
            var textNormalize = this.Prev_Chapter.Normalize(NormalizationForm.FormD);
            string textReplacePrevhapter = reg.Replace(textNormalize, "");

            var connection = await this.apiService.CheckConnection();
            if (!connection.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    connection.Message,
                    "Accept");
                return;
            }

            var response = await this.apiService.GetList<Content>(
                "http://api.biblesupersearch.com",
                "/api",
                string.Format(
                    "?bible={0}&reference={1}",
                    MainViewModel.GetInstance().SelectedModule,
                    textReplacePrevhapter));

            if (!response.IsSuccess)
            {
                this.IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }

            this.contentList = (List<Content>)response.Result;
            var contentResult = contentList[0].ContentResults[0];

            var type_bibles = typeof(VerseResult);
            var properties_bibles = type_bibles.GetRuntimeFields();
            Quote _Quote = null;

            foreach (var property in properties_bibles)
            {
                var code = property.Name.Substring(1, 3);
                _Quote = (Quote)property.GetValue(contentResult.Verses);
                if (_Quote != null)
                {
                    break;
                }
            }

            if (_Quote == null)
            {
                return;
            }

            var type_chapter = typeof(Chapter);
            var properties_chapter = type_chapter.GetRuntimeFields();
            this.myVerses = new List<BaseVerse>();
            foreach (var property in properties_chapter)
            {
                if (this.P_Chapter == "1")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter1);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "2")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter2);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "3")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter3);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "4")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter4);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "5")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter5);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "6")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter6);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "7")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter7);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "8")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter8);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "9")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter9);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "10")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter10);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "11")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter11);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "12")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter12);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "13")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter13);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "14")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter14);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "15")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter15);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "16")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter16);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "17")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter17);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "18")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter18);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "19")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter19);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "20")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter20);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "21")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter21);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "22")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter22);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "23")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter23);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "24")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter24);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "25")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter25);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "26")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter26);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "27")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter27);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "28")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter28);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "29")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter29);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "30")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter30);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "31")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter31);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "32")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter32);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "33")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter33);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "34")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter34);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "35")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter35);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "36")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter36);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "37")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter37);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "38")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter38);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "39")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter39);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "40")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter40);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "41")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter41);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "42")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter42);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "43")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter43);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "44")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter44);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "45")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter45);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "46")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter46);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "47")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter47);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "48")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter48);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "49")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter49);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "50")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter50);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "51")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter51);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "52")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter52);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.N_Chapter == "53")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter53);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "54")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter54);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "55")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter55);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "56")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter56);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "57")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter57);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "58")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter58);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "59")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter59);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "60")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter60);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "61")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter61);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "62")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter62);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "63")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter63);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "64")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter64);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "65")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter65);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "66")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter66);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "67")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter67);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "68")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter68);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "69")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter69);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "70")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter70);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "71")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter71);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "72")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter72);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "73")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter73);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "74")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter74);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "75")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter75);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "76")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter76);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "77")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter77);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "78")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter78);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "79")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter79);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "80")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter80);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "81")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter81);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "82")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter82);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "83")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter83);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "84")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter84);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "85")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter85);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "86")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter86);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "87")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter87);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "88")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter88);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "89")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter89);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "90")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter90);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "91")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter91);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "92")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter92);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "93")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter93);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "94")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter94);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "95")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter95);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "96")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter96);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "97")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter97);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "98")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter98);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "99")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter99);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }

                if (this.P_Chapter == "100")
                {
                    var verse = (BaseVerse)property.GetValue(_Quote.Chapter100);
                    if (verse != null)
                    {
                        this.myVerses.Add(verse);
                    }
                }
            }

            this.Verses = new ObservableCollection<BaseVerse>(this.myVerses);
            this.IsRefreshing = false;

            this.P_Chapter = contentList[0].ContentResults[0].Nav.PChapter;
            this.N_Chapter = contentList[0].ContentResults[0].Nav.NChapter;
            this.Prev_Chapter = contentList[0].ContentResults[0].Nav.PrevChapter;
            this.Next_Chapter = contentList[0].ContentResults[0].Nav.NextChapter;
            var p_Chapter_int = Int32.Parse(P_Chapter);
            var n_Chapter_int = Int32.Parse(N_Chapter);

            if (this.Prev_Chapter == null)
            {
                this.IsEnabledBack = false;
            }
            else if (p_Chapter_int > n_Chapter_int)
            {
                this.IsEnabledBack = false;
            }
            else
            {
                this.IsEnabledBack = true;
            }

            if (this.Next_Chapter == null)
            {
                this.IsEnabledNext = false;
            }
            else
            {
                this.IsEnabledNext = true;
            }
        }
        #endregion

        #region Commands
        public ICommand BackChapter
        {
            get
            {
                return new RelayCommand(LoadChapterBack);
            }
        }

        public ICommand NextChapter
        {
            get
            {
                return new RelayCommand(LoadChapterNext);
            }
        }
        #endregion
    }
}