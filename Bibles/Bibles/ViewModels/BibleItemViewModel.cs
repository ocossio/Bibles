﻿namespace Bibles.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Bibles.Models;
    using System.Windows.Input;
    using Bibles.Views;
    using Xamarin.Forms;

    public class BibleItemViewModel : Bible
    {
        #region Commands
        public ICommand SelectBibleCommand
        {
            get
            {
                return new RelayCommand(SelectBible);
            }
        }
        #endregion

        #region Methods
        private async void SelectBible()
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Books = new BooksViewModel(this);
            mainViewModel.SelectedModule = Module;
            mainViewModel.SelectedShortName = ShortName;
            mainViewModel.SelectChapter = "1";
            await App.Navigator.PushAsync(new BooksPage());
        }
        #endregion
    }
}