﻿namespace Bibles.ViewModels
{
    using System.Windows.Input;
    using Bibles.Helpers;
    using Bibles.Views;
    using GalaSoft.MvvmLight.Command;
    using Xamarin.Forms;

    public class MenuItemViewModel
    {
        #region Properties
        public string Icon { get; set; }

        public string Title { get; set; }

        public string PageName { get; set; }
        #endregion

        #region Methods
        private async void Navigate()
        {
            if (this.PageName == "LoginPage")
            {
                Settings.Token = string.Empty;
                Settings.TokenType = string.Empty;
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.Token = string.Empty;
                mainViewModel.TokenType = string.Empty;
                Application.Current.MainPage = new NavigationPage(new LoginPage())
                {
                    BarBackgroundColor = Color.FromHex("#2c3e50"),
                    BarTextColor = Color.White,
                };
            }

            if (this.PageName == "SearchOnePage")
            {
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.SearchOne = new SearchOneViewModel();
                await App.Navigator.PushAsync(new SearchOnePage());
            }

            if (this.PageName == "SearchTwoPage")
            {
                var mainViewModel = MainViewModel.GetInstance();
                mainViewModel.SearchTwo = new SearchTwoViewModel();
                await App.Navigator.PushAsync(new SearchTwoPage());
            }
        }
        #endregion

        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }
        #endregion
    }
}

