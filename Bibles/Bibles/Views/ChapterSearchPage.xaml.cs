﻿namespace Bibles.Views
{
    using Xamarin.Forms;

    public partial class ChapterSearchPage : ContentPage
    {
        public ChapterSearchPage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
        }
    }
}
