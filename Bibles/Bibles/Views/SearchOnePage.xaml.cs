﻿namespace Bibles.Views
{
    using System.Reflection;
    using Bibles.ViewModels;
    using Xamarin.Forms;

    public partial class SearchOnePage : ContentPage
    {
        #region Attributes
        private string selectedModule;
        private string SelectedLangShort;
        #endregion

        #region Constructors
        public SearchOnePage()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        void Handle_SelectedBiblesIndexChanged(object sender, System.EventArgs e)
        {
            var searchOneViewModel = SearchOneViewModel.GetInstance();

            var SelectBible = PickerBibles.SelectedItem;

            foreach (PropertyInfo propertyInfo in SelectBible.GetType().GetProperties())
            {
                if (propertyInfo.Name == "Module")
                {
                    var value = propertyInfo.GetValue(SelectBible, null);
                    searchOneViewModel.SelectedModule = value.ToString();
                    this.selectedModule = value.ToString();
                }

                if (propertyInfo.Name == "Lang_Short")
                {
                    var value = propertyInfo.GetValue(SelectBible, null);
                    searchOneViewModel.LangShort = value.ToString();
                    this.SelectedLangShort = value.ToString();
                }
            }

            searchOneViewModel.SearchOne = new SearchOneViewModel(PickerBooks, 
                                                                  this.selectedModule, 
                                                                  this.SelectedLangShort);
        }

        void Handle_SelectedBooksIndexChanged(object sender, System.EventArgs e)
        {
            var searchOneViewModel = SearchOneViewModel.GetInstance();

            var SelectBook = PickerBooks.SelectedItem;

            foreach (PropertyInfo propertyInfo in SelectBook.GetType().GetProperties())
            {
                if (propertyInfo.Name == "Name")
                {
                    var value = propertyInfo.GetValue(SelectBook, null);
                    searchOneViewModel.SelectBook = value.ToString();
                    this.selectedModule = value.ToString();
                }

                if (propertyInfo.Name == "ShortName")
                {
                    var value = propertyInfo.GetValue(SelectBook, null);
                    searchOneViewModel.SelectedShortName = value.ToString();
                    this.SelectedLangShort = value.ToString();
                }
            }
        }
        #endregion
    }
}
