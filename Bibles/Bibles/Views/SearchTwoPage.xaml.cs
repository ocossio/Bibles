﻿namespace Bibles.Views
{
    using System.Collections.ObjectModel;
    using System.Reflection;
    using Bibles.ViewModels;
    using Xamarin.Forms;

    public partial class SearchTwoPage : ContentPage
    {
        #region Attributes
        private string selectedModule;
        private string SelectedLangShort;
        #endregion

        #region Contructors
        public SearchTwoPage()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        void Handle_SelectedBiblesIndexChanged(object sender, System.EventArgs e)
        {
            var searchTwoViewModel = SearchTwoViewModel.GetInstance();

            var SelectBible = PickerBibles.SelectedItem;

            foreach (PropertyInfo propertyInfo in SelectBible.GetType().GetProperties())
            {
                if (propertyInfo.Name == "Module")
                {
                    var value = propertyInfo.GetValue(SelectBible, null);
                    searchTwoViewModel.SelectedModule = value.ToString();
                    this.selectedModule = value.ToString();
                }

                if (propertyInfo.Name == "Lang_Short")
                {
                    var value = propertyInfo.GetValue(SelectBible, null);
                    searchTwoViewModel.SelectedLangShort = value.ToString();
                    this.SelectedLangShort = value.ToString();
                }
            }

            searchTwoViewModel.SearchTwo = new SearchTwoViewModel(PickerBooks, 
                                                                  this.selectedModule, 
                                                                  this.SelectedLangShort);
        }

        void Handle_SelectedBooksIndexChanged(object sender, System.EventArgs e)
        {
            //if (PickerBooks.SelectedItem.Equals("(Not available)"))
            //{
            //    var empty = new ObservableCollection<Picker>();
            //    PickerBooks.ItemsSource = empty;
            //    Application.Current.MainPage.DisplayAlert(
            //        "Error",
            //        "Select first a Bible ",
            //        "Accept");
            //}

            var searchTwoViewModel = SearchTwoViewModel.GetInstance();

            var SelectBook = PickerBooks.SelectedItem;

            foreach (PropertyInfo propertyInfo in SelectBook.GetType().GetProperties())
            {
                if (propertyInfo.Name == "Name")
                {
                    var value = propertyInfo.GetValue(SelectBook, null);
                    searchTwoViewModel.SelectBook = value.ToString();
                }

                if (propertyInfo.Name == "ShortName")
                {
                    var value = propertyInfo.GetValue(SelectBook, null);
                    searchTwoViewModel.SelectedShortName = value.ToString();
                }
            }
        }
        #endregion
    }
}
