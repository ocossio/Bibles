﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Bibles.Views
{
    public partial class KeywordSearchPage : ContentPage
    {
        public KeywordSearchPage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            this.Navigation.RemovePage(this.Navigation.NavigationStack[this.Navigation.NavigationStack.Count - 2]);
        }
    }
}
