﻿namespace Bibles.Models
{
    using Newtonsoft.Json;

    public class Bible
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "shortname")]
        public string ShortName { get; set; }

        [JsonProperty(PropertyName = "module")]
        public string Module { get; set; }

        [JsonProperty(PropertyName = "year")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "lang")]
        public string Lang { get; set; }

        [JsonProperty(PropertyName = "lang_short")]
        public string Lang_Short { get; set; }

        [JsonProperty(PropertyName = "copyright")]
        public int Copyright { get; set; }

        [JsonProperty(PropertyName = "italics")]
        public int Italics { get; set; }

        [JsonProperty(PropertyName = "strongs")]
        public int Strongs { get; set; }

        [JsonProperty(PropertyName = "rank")]
        public int Rank { get; set; }

        [JsonProperty(PropertyName = "research")]
        public int Research { get; set; }
    }
}

