﻿namespace Bibles.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Books
    {
        [JsonProperty(PropertyName = "errors")]
        public List<object> errors { get; set; }

        [JsonProperty(PropertyName = "error_level")]
        public int Error_Level { get; set; }

        [JsonProperty(PropertyName = "results")]
        public List<Book> Results { get; set; }
    }
}

