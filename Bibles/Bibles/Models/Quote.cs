﻿namespace Bibles.Models
{
    using Newtonsoft.Json;

    public class Quote 
    {
        [JsonProperty(PropertyName = "1")]
        public Chapter Chapter1 { get; set; }

        [JsonProperty(PropertyName = "2")]
        public Chapter Chapter2 { get; set; }

        [JsonProperty(PropertyName = "3")]
        public Chapter Chapter3 { get; set; }

        [JsonProperty(PropertyName = "4")]
        public Chapter Chapter4 { get; set; }

        [JsonProperty(PropertyName = "5")]
        public Chapter Chapter5 { get; set; }

        [JsonProperty(PropertyName = "6")]
        public Chapter Chapter6 { get; set; }

        [JsonProperty(PropertyName = "7")]
        public Chapter Chapter7 { get; set; }

        [JsonProperty(PropertyName = "8")]
        public Chapter Chapter8 { get; set; }

        [JsonProperty(PropertyName = "9")]
        public Chapter Chapter9 { get; set; }

        [JsonProperty(PropertyName = "10")]
        public Chapter Chapter10 { get; set; }

        [JsonProperty(PropertyName = "11")]
        public Chapter Chapter11 { get; set; }

        [JsonProperty(PropertyName = "12")]
        public Chapter Chapter12 { get; set; }

        [JsonProperty(PropertyName = "13")]
        public Chapter Chapter13 { get; set; }

        [JsonProperty(PropertyName = "14")]
        public Chapter Chapter14 { get; set; }

        [JsonProperty(PropertyName = "15")]
        public Chapter Chapter15 { get; set; }

        [JsonProperty(PropertyName = "16")]
        public Chapter Chapter16 { get; set; }

        [JsonProperty(PropertyName = "17")]
        public Chapter Chapter17 { get; set; }

        [JsonProperty(PropertyName = "18")]
        public Chapter Chapter18 { get; set; }

        [JsonProperty(PropertyName = "19")]
        public Chapter Chapter19 { get; set; }

        [JsonProperty(PropertyName = "20")]
        public Chapter Chapter20 { get; set; }

        [JsonProperty(PropertyName = "21")]
        public Chapter Chapter21 { get; set; }

        [JsonProperty(PropertyName = "22")]
        public Chapter Chapter22 { get; set; }

        [JsonProperty(PropertyName = "23")]
        public Chapter Chapter23 { get; set; }

        [JsonProperty(PropertyName = "24")]
        public Chapter Chapter24 { get; set; }

        [JsonProperty(PropertyName = "25")]
        public Chapter Chapter25 { get; set; }

        [JsonProperty(PropertyName = "26")]
        public Chapter Chapter26 { get; set; }

        [JsonProperty(PropertyName = "27")]
        public Chapter Chapter27 { get; set; }

        [JsonProperty(PropertyName = "28")]
        public Chapter Chapter28 { get; set; }

        [JsonProperty(PropertyName = "29")]
        public Chapter Chapter29 { get; set; }

        [JsonProperty(PropertyName = "30")]
        public Chapter Chapter30 { get; set; }

        [JsonProperty(PropertyName = "31")]
        public Chapter Chapter31 { get; set; }

        [JsonProperty(PropertyName = "32")]
        public Chapter Chapter32 { get; set; }

        [JsonProperty(PropertyName = "33")]
        public Chapter Chapter33 { get; set; }

        [JsonProperty(PropertyName = "34")]
        public Chapter Chapter34 { get; set; }

        [JsonProperty(PropertyName = "35")]
        public Chapter Chapter35 { get; set; }

        [JsonProperty(PropertyName = "36")]
        public Chapter Chapter36 { get; set; }

        [JsonProperty(PropertyName = "37")]
        public Chapter Chapter37 { get; set; }

        [JsonProperty(PropertyName = "38")]
        public Chapter Chapter38 { get; set; }

        [JsonProperty(PropertyName = "39")]
        public Chapter Chapter39 { get; set; }

        [JsonProperty(PropertyName = "40")]
        public Chapter Chapter40 { get; set; }

        [JsonProperty(PropertyName = "41")]
        public Chapter Chapter41 { get; set; }

        [JsonProperty(PropertyName = "42")]
        public Chapter Chapter42 { get; set; }

        [JsonProperty(PropertyName = "43")]
        public Chapter Chapter43 { get; set; }

        [JsonProperty(PropertyName = "44")]
        public Chapter Chapter44 { get; set; }

        [JsonProperty(PropertyName = "45")]
        public Chapter Chapter45 { get; set; }

        [JsonProperty(PropertyName = "46")]
        public Chapter Chapter46 { get; set; }

        [JsonProperty(PropertyName = "47")]
        public Chapter Chapter47 { get; set; }

        [JsonProperty(PropertyName = "48")]
        public Chapter Chapter48 { get; set; }

        [JsonProperty(PropertyName = "49")]
        public Chapter Chapter49 { get; set; }

        [JsonProperty(PropertyName = "50")]
        public Chapter Chapter50 { get; set; }

        [JsonProperty(PropertyName = "51")]
        public Chapter Chapter51 { get; set; }

        [JsonProperty(PropertyName = "52")]
        public Chapter Chapter52 { get; set; }

        [JsonProperty(PropertyName = "53")]
        public Chapter Chapter53 { get; set; }

        [JsonProperty(PropertyName = "54")]
        public Chapter Chapter54 { get; set; }

        [JsonProperty(PropertyName = "55")]
        public Chapter Chapter55 { get; set; }

        [JsonProperty(PropertyName = "56")]
        public Chapter Chapter56 { get; set; }

        [JsonProperty(PropertyName = "57")]
        public Chapter Chapter57 { get; set; }

        [JsonProperty(PropertyName = "58")]
        public Chapter Chapter58 { get; set; }

        [JsonProperty(PropertyName = "59")]
        public Chapter Chapter59 { get; set; }

        [JsonProperty(PropertyName = "60")]
        public Chapter Chapter60 { get; set; }

        [JsonProperty(PropertyName = "61")]
        public Chapter Chapter61 { get; set; }

        [JsonProperty(PropertyName = "62")]
        public Chapter Chapter62 { get; set; }

        [JsonProperty(PropertyName = "63")]
        public Chapter Chapter63 { get; set; }

        [JsonProperty(PropertyName = "64")]
        public Chapter Chapter64 { get; set; }

        [JsonProperty(PropertyName = "65")]
        public Chapter Chapter65 { get; set; }

        [JsonProperty(PropertyName = "66")]
        public Chapter Chapter66 { get; set; }

        [JsonProperty(PropertyName = "67")]
        public Chapter Chapter67 { get; set; }

        [JsonProperty(PropertyName = "68")]
        public Chapter Chapter68 { get; set; }

        [JsonProperty(PropertyName = "69")]
        public Chapter Chapter69 { get; set; }

        [JsonProperty(PropertyName = "70")]
        public Chapter Chapter70 { get; set; }

        [JsonProperty(PropertyName = "71")]
        public Chapter Chapter71 { get; set; }

        [JsonProperty(PropertyName = "72")]
        public Chapter Chapter72 { get; set; }

        [JsonProperty(PropertyName = "73")]
        public Chapter Chapter73 { get; set; }

        [JsonProperty(PropertyName = "74")]
        public Chapter Chapter74 { get; set; }

        [JsonProperty(PropertyName = "75")]
        public Chapter Chapter75 { get; set; }

        [JsonProperty(PropertyName = "76")]
        public Chapter Chapter76 { get; set; }

        [JsonProperty(PropertyName = "77")]
        public Chapter Chapter77 { get; set; }

        [JsonProperty(PropertyName = "78")]
        public Chapter Chapter78 { get; set; }

        [JsonProperty(PropertyName = "79")]
        public Chapter Chapter79 { get; set; }

        [JsonProperty(PropertyName = "80")]
        public Chapter Chapter80 { get; set; }

        [JsonProperty(PropertyName = "81")]
        public Chapter Chapter81 { get; set; }

        [JsonProperty(PropertyName = "82")]
        public Chapter Chapter82 { get; set; }

        [JsonProperty(PropertyName = "83")]
        public Chapter Chapter83 { get; set; }

        [JsonProperty(PropertyName = "84")]
        public Chapter Chapter84 { get; set; }

        [JsonProperty(PropertyName = "85")]
        public Chapter Chapter85 { get; set; }

        [JsonProperty(PropertyName = "86")]
        public Chapter Chapter86 { get; set; }

        [JsonProperty(PropertyName = "87")]
        public Chapter Chapter87 { get; set; }

        [JsonProperty(PropertyName = "88")]
        public Chapter Chapter88 { get; set; }

        [JsonProperty(PropertyName = "89")]
        public Chapter Chapter89 { get; set; }

        [JsonProperty(PropertyName = "90")]
        public Chapter Chapter90 { get; set; }

        [JsonProperty(PropertyName = "91")]
        public Chapter Chapter91 { get; set; }

        [JsonProperty(PropertyName = "92")]
        public Chapter Chapter92 { get; set; }

        [JsonProperty(PropertyName = "93")]
        public Chapter Chapter93 { get; set; }

        [JsonProperty(PropertyName = "94")]
        public Chapter Chapter94 { get; set; }

        [JsonProperty(PropertyName = "95")]
        public Chapter Chapter95 { get; set; }

        [JsonProperty(PropertyName = "96")]
        public Chapter Chapter96 { get; set; }

        [JsonProperty(PropertyName = "97")]
        public Chapter Chapter97 { get; set; }

        [JsonProperty(PropertyName = "98")]
        public Chapter Chapter98 { get; set; }

        [JsonProperty(PropertyName = "99")]
        public Chapter Chapter99 { get; set; }

        [JsonProperty(PropertyName = "100")]
        public Chapter Chapter100 { get; set; }
    }
}


