﻿using Newtonsoft.Json;

namespace Bibles.Models
{
    public class Chapter
    {
        [JsonProperty(PropertyName = "1")]
        public BaseVerse Verse1 { get; set; }

        [JsonProperty(PropertyName = "2")]
        public BaseVerse Verse2 { get; set; }

        [JsonProperty(PropertyName = "3")]
        public BaseVerse Verse3 { get; set; }

        [JsonProperty(PropertyName = "4")]
        public BaseVerse Verse4 { get; set; }

        [JsonProperty(PropertyName = "5")]
        public BaseVerse Verse5 { get; set; }

        [JsonProperty(PropertyName = "6")]
        public BaseVerse Verse6 { get; set; }

        [JsonProperty(PropertyName = "7")]
        public BaseVerse Verse7 { get; set; }

        [JsonProperty(PropertyName = "8")]
        public BaseVerse Verse8 { get; set; }

        [JsonProperty(PropertyName = "9")]
        public BaseVerse Verse9 { get; set; }

        [JsonProperty(PropertyName = "10")]
        public BaseVerse Verse10 { get; set; }

        [JsonProperty(PropertyName = "11")]
        public BaseVerse Verse11 { get; set; }

        [JsonProperty(PropertyName = "12")]
        public BaseVerse Verse12 { get; set; }

        [JsonProperty(PropertyName = "13")]
        public BaseVerse Verse13 { get; set; }

        [JsonProperty(PropertyName = "14")]
        public BaseVerse Verse14 { get; set; }

        [JsonProperty(PropertyName = "15")]
        public BaseVerse Verse15 { get; set; }

        [JsonProperty(PropertyName = "16")]
        public BaseVerse Verse16 { get; set; }

        [JsonProperty(PropertyName = "17")]
        public BaseVerse Verse17 { get; set; }

        [JsonProperty(PropertyName = "18")]
        public BaseVerse Verse18 { get; set; }

        [JsonProperty(PropertyName = "19")]
        public BaseVerse Verse19 { get; set; }

        [JsonProperty(PropertyName = "20")]
        public BaseVerse Verse20 { get; set; }

        [JsonProperty(PropertyName = "21")]
        public BaseVerse Verse21 { get; set; }

        [JsonProperty(PropertyName = "22")]
        public BaseVerse Verse22 { get; set; }

        [JsonProperty(PropertyName = "23")]
        public BaseVerse Verse23 { get; set; }

        [JsonProperty(PropertyName = "24")]
        public BaseVerse Verse24 { get; set; }

        [JsonProperty(PropertyName = "25")]
        public BaseVerse Verse25 { get; set; }

        [JsonProperty(PropertyName = "26")]
        public BaseVerse Verse26 { get; set; }

        [JsonProperty(PropertyName = "27")]
        public BaseVerse Verse27 { get; set; }

        [JsonProperty(PropertyName = "28")]
        public BaseVerse Verse28 { get; set; }

        [JsonProperty(PropertyName = "29")]
        public BaseVerse Verse29 { get; set; }

        [JsonProperty(PropertyName = "30")]
        public BaseVerse Verse30 { get; set; }

        [JsonProperty(PropertyName = "31")]
        public BaseVerse Verse31 { get; set; }

        [JsonProperty(PropertyName = "32")]
        public BaseVerse Verse32 { get; set; }

        [JsonProperty(PropertyName = "33")]
        public BaseVerse Verse33 { get; set; }

        [JsonProperty(PropertyName = "34")]
        public BaseVerse Verse34 { get; set; }

        [JsonProperty(PropertyName = "35")]
        public BaseVerse Verse35 { get; set; }

        [JsonProperty(PropertyName = "36")]
        public BaseVerse Verse36 { get; set; }

        [JsonProperty(PropertyName = "37")]
        public BaseVerse Verse37 { get; set; }

        [JsonProperty(PropertyName = "38")]
        public BaseVerse Verse38 { get; set; }

        [JsonProperty(PropertyName = "39")]
        public BaseVerse Verse39 { get; set; }

        [JsonProperty(PropertyName = "40")]
        public BaseVerse Verse40 { get; set; }

        [JsonProperty(PropertyName = "41")]
        public BaseVerse Verse41 { get; set; }

        [JsonProperty(PropertyName = "42")]
        public BaseVerse Verse42 { get; set; }

        [JsonProperty(PropertyName = "43")]
        public BaseVerse Verse43 { get; set; }

        [JsonProperty(PropertyName = "44")]
        public BaseVerse Verse44 { get; set; }

        [JsonProperty(PropertyName = "45")]
        public BaseVerse Verse45 { get; set; }

        [JsonProperty(PropertyName = "46")]
        public BaseVerse Verse46 { get; set; }

        [JsonProperty(PropertyName = "47")]
        public BaseVerse Verse47 { get; set; }

        [JsonProperty(PropertyName = "48")]
        public BaseVerse Verse48 { get; set; }

        [JsonProperty(PropertyName = "49")]
        public BaseVerse Verse49 { get; set; }

        [JsonProperty(PropertyName = "50")]
        public BaseVerse Verse50 { get; set; }

        [JsonProperty(PropertyName = "51")]
        public BaseVerse Verse51 { get; set; }

        [JsonProperty(PropertyName = "52")]
        public BaseVerse Verse52 { get; set; }

        [JsonProperty(PropertyName = "53")]
        public BaseVerse Verse53 { get; set; }

        [JsonProperty(PropertyName = "54")]
        public BaseVerse Verse54 { get; set; }

        [JsonProperty(PropertyName = "55")]
        public BaseVerse Verse55 { get; set; }

        [JsonProperty(PropertyName = "56")]
        public BaseVerse Verse56 { get; set; }

        [JsonProperty(PropertyName = "57")]
        public BaseVerse Verse57 { get; set; }

        [JsonProperty(PropertyName = "58")]
        public BaseVerse Verse58 { get; set; }

        [JsonProperty(PropertyName = "59")]
        public BaseVerse Verse59 { get; set; }

        [JsonProperty(PropertyName = "60")]
        public BaseVerse Verse60 { get; set; }

        [JsonProperty(PropertyName = "61")]
        public BaseVerse Verse61 { get; set; }

        [JsonProperty(PropertyName = "62")]
        public BaseVerse Verse62 { get; set; }

        [JsonProperty(PropertyName = "63")]
        public BaseVerse Verse63 { get; set; }

        [JsonProperty(PropertyName = "64")]
        public BaseVerse Verse64 { get; set; }

        [JsonProperty(PropertyName = "65")]
        public BaseVerse Verse65 { get; set; }

        [JsonProperty(PropertyName = "66")]
        public BaseVerse Verse66 { get; set; }

        [JsonProperty(PropertyName = "67")]
        public BaseVerse Verse67 { get; set; }

        [JsonProperty(PropertyName = "68")]
        public BaseVerse Verse68 { get; set; }

        [JsonProperty(PropertyName = "69")]
        public BaseVerse Verse69 { get; set; }

        [JsonProperty(PropertyName = "70")]
        public BaseVerse Verse70 { get; set; }

        [JsonProperty(PropertyName = "71")]
        public BaseVerse Verse71 { get; set; }

        [JsonProperty(PropertyName = "72")]
        public BaseVerse Verse72 { get; set; }

        [JsonProperty(PropertyName = "73")]
        public BaseVerse Verse73 { get; set; }

        [JsonProperty(PropertyName = "74")]
        public BaseVerse Verse74 { get; set; }

        [JsonProperty(PropertyName = "75")]
        public BaseVerse Verse75 { get; set; }

        [JsonProperty(PropertyName = "76")]
        public BaseVerse Verse76 { get; set; }

        [JsonProperty(PropertyName = "77")]
        public BaseVerse Verse77 { get; set; }

        [JsonProperty(PropertyName = "78")]
        public BaseVerse Verse78 { get; set; }

        [JsonProperty(PropertyName = "79")]
        public BaseVerse Verse79 { get; set; }

        [JsonProperty(PropertyName = "80")]
        public BaseVerse Verse80 { get; set; }

        [JsonProperty(PropertyName = "81")]
        public BaseVerse Verse81 { get; set; }

        [JsonProperty(PropertyName = "82")]
        public BaseVerse Verse82 { get; set; }

        [JsonProperty(PropertyName = "83")]
        public BaseVerse Verse83 { get; set; }

        [JsonProperty(PropertyName = "84")]
        public BaseVerse Verse84 { get; set; }

        [JsonProperty(PropertyName = "85")]
        public BaseVerse Verse85 { get; set; }

        [JsonProperty(PropertyName = "86")]
        public BaseVerse Verse86 { get; set; }

        [JsonProperty(PropertyName = "87")]
        public BaseVerse Verse87 { get; set; }

        [JsonProperty(PropertyName = "88")]
        public BaseVerse Verse88 { get; set; }

        [JsonProperty(PropertyName = "89")]
        public BaseVerse Verse89 { get; set; }

        [JsonProperty(PropertyName = "90")]
        public BaseVerse Verse90 { get; set; }

        [JsonProperty(PropertyName = "91")]
        public BaseVerse Verse91 { get; set; }

        [JsonProperty(PropertyName = "92")]
        public BaseVerse Verse92 { get; set; }

        [JsonProperty(PropertyName = "93")]
        public BaseVerse Verse93 { get; set; }

        [JsonProperty(PropertyName = "94")]
        public BaseVerse Verse94 { get; set; }

        [JsonProperty(PropertyName = "95")]
        public BaseVerse Verse95 { get; set; }

        [JsonProperty(PropertyName = "96")]
        public BaseVerse Verse96 { get; set; }

        [JsonProperty(PropertyName = "97")]
        public BaseVerse Verse97 { get; set; }

        [JsonProperty(PropertyName = "98")]
        public BaseVerse Verse98 { get; set; }

        [JsonProperty(PropertyName = "99")]
        public BaseVerse Verse99 { get; set; }

        [JsonProperty(PropertyName = "100")]
        public BaseVerse Verse100 { get; set; }

        [JsonProperty(PropertyName = "101")]
        public BaseVerse Verse101 { get; set; }

    }
}