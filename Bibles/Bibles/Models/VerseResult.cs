﻿namespace Bibles.Models
{
    using Newtonsoft.Json;

    public class VerseResult
    {
        [JsonProperty(PropertyName = "kjv")]
        public Quote Kjv { get; set; }

        [JsonProperty(PropertyName = "kjv_strongs")]
        public Quote KjvStrongs { get; set; }

        [JsonProperty(PropertyName = "tyndale")]
        public Quote Tyndale { get; set; }

        [JsonProperty(PropertyName = "coverdale")]
        public Quote Coverdale { get; set; }

        [JsonProperty(PropertyName = "bishops")]
        public Quote Bishops { get; set; }

        [JsonProperty(PropertyName = "geneva")]
        public Quote Geneva { get; set; }

        [JsonProperty(PropertyName = "tr")]
        public Quote Tr { get; set; }

        [JsonProperty(PropertyName = "trparsed")]
        public Quote Trparsed { get; set; }

        [JsonProperty(PropertyName = "rv_1858")]
        public Quote Rv1858 { get; set; }

        [JsonProperty(PropertyName = "rv_1909")]
        public Quote Rv1909 { get; set; }

        [JsonProperty(PropertyName = "sagradas")]
        public Quote Sagradas { get; set; }

        [JsonProperty(PropertyName = "rvg")]
        public Quote Rvg { get; set; }

        [JsonProperty(PropertyName = "martin")]
        public Quote Martin { get; set; }

        [JsonProperty(PropertyName = "epee")]
        public Quote Epee { get; set; }

        [JsonProperty(PropertyName = "oster")]
        public Quote Oster { get; set; }

        [JsonProperty(PropertyName = "afri")]
        public Quote Afri { get; set; }

        [JsonProperty(PropertyName = "svd")]
        public Quote Svd { get; set; }

        [JsonProperty(PropertyName = "bkr")]
        public Quote Bkr { get; set; }

        [JsonProperty(PropertyName = "stve")]
        public Quote Stve { get; set; }

        [JsonProperty(PropertyName = "finn")]
        public Quote Finn { get; set; }

        [JsonProperty(PropertyName = "luther")]
        public Quote Luther { get; set; }

        [JsonProperty(PropertyName = "diodati")]
        public Quote Diodati { get; set; }

        [JsonProperty(PropertyName = "synodal")]
        public Quote Synodal { get; set; }

        [JsonProperty(PropertyName = "karoli")]
        public Quote Karoli { get; set; }

        [JsonProperty(PropertyName = "lith")]
        public Quote Lith { get; set; }

        [JsonProperty(PropertyName = "maori")]
        public Quote Maori { get; set; }

        [JsonProperty(PropertyName = "cornilescu")]
        public Quote Cornilescu { get; set; }

        [JsonProperty(PropertyName = "thaikjv")]
        public Quote Thaikjv { get; set; }

        [JsonProperty(PropertyName = "wlc")]
        public Quote Wlc { get; set; }
    }
}

