﻿namespace Bibles.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class Bibles
    {
        [JsonProperty(PropertyName = "errors")]
        public List<object> Errors { get; set; }

        [JsonProperty(PropertyName = "error_level")]
        public int Error_Level { get; set; }

        [JsonProperty(PropertyName = "results")]
        public BiblesResults Results { get; set; }
    }
}

