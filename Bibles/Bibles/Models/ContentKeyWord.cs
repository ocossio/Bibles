﻿namespace Bibles.Models
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class ContentKeyWord
    {
        [JsonProperty(PropertyName = "errors")]
        public List<object> Errors { get; set; }

        [JsonProperty(PropertyName = "error_level")]
        public int ErrorLevel { get; set; }

        [JsonProperty(PropertyName = "results")]
        public List<BooksKeyWord> ContentResultsKeyWord { get; set; }
    }
}

